# Introduction

This repo contains two very basic examples of string interoperativity
between C & C# code using [Marshal](https://docs.microsoft.com/en-us/dotnet/framework/interop/interop-marshaling).

It contains two examples:

  * Passing C strings to C# code.
  * Passing C# strings to C code.

In order to run it you need [.NET Core](https://dotnet.microsoft.com/) & a C compiler.

In macOS you can run the example with the provided [`run.sh`](run.sh) script.
In other systems, compile the C code into a dynamic library named `c_code.[dll,so,*]`,
move that into the [`cs_code`](cs_code) folder and there run ```dotnet run```.

﻿using System;
using System.Runtime.InteropServices;

namespace cs_code
{
    class Program
    {
        [DllImport("c_code", EntryPoint = "function_returning_a_char_pointer", CharSet = CharSet.Ansi)]
        [return: MarshalAs(UnmanagedType.LPStr)]
        private static extern string get_string_from_c();

        [DllImport("c_code", EntryPoint = "function_printing_two_char_arrays", CharSet = CharSet.Ansi)]
        private static extern void print_strings_in_c(string str1, string str2);

        static void Main(string[] args)
        {
            Console.WriteLine($"Printing from C# code:: <<{get_string_from_c()}>>");
            print_strings_in_c("This string was generated", "in C# code");
        }
    }
}